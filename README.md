# Todaystask
A simple command-line daily task manager

## Pre-requirements

- dart: ^2.5.0

## How to setup

```bash
$ git clone https://gitlab.com/miyazakijunichi/todaystask.git
$ pub get
$ dart2aot bin/main.dart build/main.dart.aot
$ cat <<EOF > ~/.local/bin/todaystask
#!/bin/bash

PROJECTDIR="$PWD"

dartaotruntime "\${PROJECTDIR}/build/main.dart.aot" \$@

EOF
$ chmod +x ~/.local/bin/todaystask
```

## How to use

```bash
$ todaystask
Task is empty. Adding tasks...
 1: write a mail to bob
 2: 
$ # print tasks
$ todaystask
 1: write a mail to bob
$ # append task by command-line
$ todaystask -a -m 'water the plants'
$ todaystask
 1: write a mail to bob
 2: water the plants
$ # append task by prompt
$ todaystask -a
 1: write a mail to bob
 2: water the plants
 3: have a tea
 4: 
```

This project was created from templates made available by Stagehand under a BSD-style
[license](https://github.com/dart-lang/stagehand/blob/master/LICENSE).
