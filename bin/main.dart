import 'dart:io';

import 'package:args/args.dart';
import 'package:path/path.dart' as p;
import 'package:todaystask/todaystask.dart' as todaystask;
import 'package:todaystask/util.dart' as util;

final targetDir = p.normalize('${Platform.environment['HOME']}/.todaystask');

ArgParser getArgParser() {
  final targetFile =
      p.join(targetDir, util.formatFileDate(DateTime.now()) + '.csv');
  final parser = ArgParser();
  parser.addFlag('append', abbr: 'a');
  parser.addOption('message', abbr: 'm');
  parser.addOption('taskfile', abbr: 'f', defaultsTo: targetFile);
  parser.addOption('done', abbr: 'd');
  parser.addOption('version', abbr: 'v');

  return parser;
}

main(List<String> arguments) async {
  final parseResult = getArgParser().parse(arguments);
  final String message = parseResult['message'];
  final String targetFile = parseResult['taskfile'];
  bool appendMode = parseResult['append'];
  bool showVersion = parseResult['version'];

  if (showVersion) {
    print('todaystack v0.0.1');
    exit(0);
  }

  if (!await Directory(targetDir).exists()) {
    await Directory(targetDir).createSync(recursive: true);
  }

  if (!await File(targetFile).exists()) {
    stderr.write('Task is empty. Adding tasks...\n');
    appendMode = true;
  }

  if (parseResult['done'] != null) {
    await todaystask.doneTask(targetFile, int.parse(parseResult['done']));
    return;
  }

  // コマンドライン引数にメッセージが指定されたらそのまま追加して終了
  if (message != null && message.isNotEmpty) {
    await todaystask.addTask(targetFile, message);
    return;
  }

  if (appendMode) {
    await todaystask.promptTask(targetFile);
  } else {
    await todaystask.printTasks(targetFile);
  }
}
