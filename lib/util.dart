
import 'dart:io';
import 'dart:math';
import 'package:intl/intl.dart';

final dateTimeFormatter = DateFormat('yyyy/MM/dd HH:mm:ss');
String formatDateTime(DateTime dateTime) {
  return dateTimeFormatter.format(dateTime);
}

final dateFormatter = DateFormat('yyyy/MM/dd');
String formatDate(DateTime dateTime) {
  return dateTimeFormatter.format(dateTime);
}

final fileDateFormatter = DateFormat('yyyy-MM-dd');
String formatFileDate(DateTime dateTime) {
  return fileDateFormatter.format(dateTime);
}

double log10(int n) => log(n) / log(10);
int digitsOfInt(int n) {
  if (n == null || n == 0) {
    return 1;
  }
  return (log10(n) + 1).floor();
}

final EOL = Platform.isWindows? '\r\n': '\n';

bool isNullOrEmpty(String s) {
  return s == null || s.isEmpty;
}
