import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:csv/csv.dart';
import 'package:sprintf/sprintf.dart';
import 'package:todaystask/util.dart' as util;

addTask(String filePath, String message) async {
  final tasks = await getTasks(filePath);
  final maxNo = tasks.map((it) => it[0] as int).fold(0, max);
  tasks.add([maxNo + 1, util.formatDateTime(DateTime.now()), message, '']);

  await writeTasks(filePath, tasks);
}

Future<List<List<dynamic>>> getTasks(String filePath) async {
  final file = File(filePath);
  if (await file.exists()) {
    final input = file.openRead();
    final result = await input
        .transform(utf8.decoder)
        .transform(CsvToListConverter(eol: util.EOL))
        .toList();
    result.removeAt(0); // ヘッダを取り除く
    return result;
  }

  return [];
}

Future<void> writeTasks(String filePath, List<List<dynamic>> tasks) async {
  String csv = const ListToCsvConverter().convert([
    ['no', 'created_at', 'message', 'done'],
    ...tasks
  ], eol: util.EOL);
  await File(filePath).writeAsString(csv);
}

Future<List<List<dynamic>>> printTasks(String filePath) async {
  final tasks = await getTasks(filePath);
  final digits = util.digitsOfInt(tasks.map((it) => it[0] as int).fold(0, max));
  for (final task in tasks) {
    final line = sprintf('% ${digits}d: %s', [task[0], task[2]]);
    stdout.write(util.isNullOrEmpty(task[3]) ? line : '\x1B[9m${line}\x1B[0m');
    stdout.write('\n');
  }

  return tasks;
}

promptTask(String filePath) async {
  final tasks = await printTasks(filePath);
  final maxNo = tasks.map((it) => it[0] as int).fold(0, max);
  final digits = util.digitsOfInt(maxNo + 1);
  var numero = maxNo + 1;

  stdout.write(sprintf('% ${digits}d: ', [numero]));
  var line = stdin.readLineSync();
  while (line.isNotEmpty) {
    tasks.add([
      numero,
      util.formatDateTime(DateTime.now()),
      line.replaceAll('\n', ''),
      ''
    ]);

    stdout.write(sprintf('% ${digits}d: ', [++numero]));
    line = stdin.readLineSync();
  }

  await writeTasks(filePath, tasks);
}

doneTask(String filePath, int n) async {
  final tasks = await getTasks(filePath);

  for (final task in tasks) {
    if (task[0] == n) {
      task[3] = 'x';
      break;
    }
  }

  await writeTasks(filePath, tasks);
}
